import webpack from 'webpack'
import path from 'path'
import autoprefixer from 'autoprefixer'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import views from './templates/entries'

let extractStyles = new ExtractTextPlugin('[name].css');
let extractHtml = new ExtractTextPlugin('[name].html');
let entry = {};

views.forEach((view) => {
  entry[view] = path.resolve(__dirname, 'templates/' + view + '.pug');
});
entry['css/application'] = [
  path.resolve(__dirname, 'assets/styles/application.scss')
];

let config = {
  stats: {
    assets: false,
    colors: true,
    version: false,
    hash: true,
    timings: true,
    chunks: false,
    chunkModules: false
  },
  performance: { hints: false },
  entry: entry,
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.pug$/,
        use: extractHtml.extract({
          use: ['html-loader', 'pug-html-loader?pretty&exports=false']
        })
      },
      {
        test: /\.scss$/,
        use: extractStyles.extract({
          use: [
            // {
            //   loader: 'style-loader'
            // },
            {
              loader: 'css-loader'
              , options: {
                module: false
                , localIdentName: '[path][name]-[local]'
                , sourceMap: true
              }
            }, {
              loader: 'postcss-loader'
              , options: {
                sourceMap: true
                , browsers: [
                  'last 3 version'
                  , 'ie >= 10'
                ]
              }
            }, {
              loader: 'sass-loader'
              , options: {
                outputStyle:  'expanded'
                , sourceMap: true
          , includePaths: [
            path.resolve(__dirname, 'node_modules/sanitize.css/')
          ]
              }
            }
          ]
        })
      },
      { test: /\.svg$/, loader: 'url?limit=65000&mimetype=image/svg+xml&name=public/fonts/[name].[ext]' },
      { test: /\.woff$/, loader: 'url?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]' },
      { test: /\.woff2$/, loader: 'url?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]' },
      { test: /\.[ot]tf$/, loader: 'url?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]' },
      { test: /\.eot$/, loader: 'url?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]' }
    ]
  },
  plugins: [
    // new webpack.LoaderOptionsPlugin({
    //   minimize: false,
    //   debug: true,
    //   options: {
    //     sourceMap: true,
    //     postcss: [
    //       autoprefixer({
    //         browsers: ['last 2 version', 'Explorer >= 10', 'Android >= 4']
    //       })
    //     ],
    //     sassLoader: {
    //       options: {sourceMap: true},
    //       includePaths: [
    //         path.resolve(__dirname, 'node_modules/sanitize.css/')
    //       ]
    //     }
    //   }
    // }),
    extractStyles,
    extractHtml
  ],
  devtool:  'cheap-module-source-map'
}

export default config
